<?php

session_start();

// database connection
require_once "connect.php";

// databasee data (host, db_user, db_password, db_name)
$connect = @new mysqli($host, $db_user, $db_password, $db_name);

// database error
if ($connect->connect_errno != 0) {
    echo "Error: " . $connect->connect_errno;
}
// if sucesful connect to database
else {
    // variables
    $login = htmlspecialchars($_POST['login']);
    $email = htmlspecialchars($_POST['email']);
    $email2 = htmlspecialchars($_POST['email2']);
    $password = htmlspecialchars($_POST['password']);
    $password2 = htmlspecialchars($_POST['password2']);
    // hashed password
    $hash_password = password_hash($password, PASSWORD_DEFAULT);

    // sql check data
    $sqlCheckLogin = "SELECT * FROM `dane` WHERE login='$login'";
    $sqlCheckEmail = "SELECT * FROM `dane` WHERE email='$email'";

    // sql insert data
    $sql = "INSERT INTO dane(`ID`, `email`, `login`, `password`) VALUES ('null','$email','$login',
        '$hash_password')";

    // ifs about repeat email and password
    // email checking
    if ($email == $email2) {

        // checking email have "@" and "." > 0
        if (strstr($email, "@") && strstr($email, ".")) {

            // password checking
            if ($password == $password2) {

                // special signs checking
                if (strstr($password, "!") || (strstr($password, "@")) || (strstr($password, "#")) || (strstr($password, "$")) || (strstr($password, "^")) || (strstr($password, "&")) || (strstr($password, "*")) || (strstr($password, "(")) || (strstr($password, ")")) || (strstr($password, "{")) || (strstr($password, "}")) || (strstr($password, "[")) || (strstr($password, "]")) || (strstr($password, ":")) || (strstr($password, ";")) || (strstr($password, "?")) || (strstr($password, "<")) || (strstr($password, ">")) || (strstr($password, ".")) || (strstr($password, ",")) || (strstr($password, "/"))) {

                    // how many sign checking
                    if (strlen($password) >= 8) {
                        // Bit sign checking
                        if (preg_match(" ~[A-Z]~", $password)) {
                            // small signs checking
                            if (preg_match(" ~[a-z]~ ", $password)) {
                                // checking numers in password
                                if (preg_match(" ~[\d]~ ", $password)) {

                                    // checking login in database
                                    if ($loginResult = @$connect->query($sqlCheckLogin)) {

                                        if ($loginResult->num_rows == 0) {
                                            // checking email in database
                                            if ($emailResult = @$connect->query($sqlCheckEmail)) {

                                                if ($emailResult->num_rows == 0) {

                                                    // add variables to database
                                                    $connect->query($sql);
                                                    // info about sucesful register
                                                    $_SESSION['createdAccount'] = "Konto stworzone pomyślnie!";
                                                    header("Location: index.php");
                                                } else {

                                                    $_SESSION['emailInfo'] = "Podany e-mail już istnieje";
                                                    header("Location: index.php");
                                                }
                                            }
                                        } else {

                                            $_SESSION['loginInfo'] = "Podany login już istnieje";
                                            header("Location: index.php");
                                        }
                                    }
                                } else {

                                    $_SESSION['passwordInfo'] = "Hasło musi mieć co najmniej 1 cyfrę!";
                                    header("Location: index.php");
                                }
                            } else {

                                $_SESSION['passwordInfo'] = "Hasło musi mieć co najmniej 1 mała literę!";
                                header("Location: index.php");
                            }
                        } else {

                            $_SESSION['passwordInfo'] = "Hasło musi mieć co najmniej 1 dużą literę!";
                            header("Location: index.php");
                        }
                    } else {

                        $_SESSION['passwordInfo'] = "Hasło musi mieć co najmniej 8 znaków!";
                        header("Location: index.php");
                    }
                } else {

                    $_SESSION['passwordInfo'] = "Hasło musi mieć co najmniej 1 znak specjalny";
                    header("Location: index.php");
                }
            } else {

                $_SESSION['passwordInfo'] = "Podane hasła nie są identyczne";
                header("Location: index.php");
            }
        } else {

            $_SESSION['emailInfo'] = "Podaj prawdziwy e-mail!";
            header("Location: index.php");
        }
    } else {

        $_SESSION['emailInfo'] = "E-maile nie są identyczne!";
        header("Location: index.php");
    }

    // database connect close
    $connect->close();
}
