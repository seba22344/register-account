<?php

// start session
session_start();
// notices off
error_reporting(0);

?>

<!DOCTYPE html>
<html lang="pl">

<head>

    <!-- meta -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Zajerestruj się</title>
    <!-- styles -->
    <style type="text/css">
        /* add styles with ph */
        <?php include "style.css"; ?>
    </style>

</head>

<body>

    <!-- register section -->
    <section id="register-section">
        <!-- container -->
        <div class="container">
            <!-- register form -->
            <div class="form">
                <form method="POST" action="register.php">
                    <!-- title -->
                    <MARQUEE width="50%" scrollamount="3">Rejestracja</MARQUEE>
                    <!-- login -->
                    <p>Wpisz login:</p>
                    <p><input type="text" name="login" placeholder="Login" required /></p>
                    <!-- php login problem alert -->
                    <?php

                    echo "<p><span style='color: #ff0000; font-weight: bold'>" . $_SESSION['loginInfo'] . "</span></p>";
                    unset($_SESSION['loginInfo']);

                    ?>

                    <!-- email -->
                    <p>Wpisz e-mail:</p>
                    <p><input type="text" name="email" placeholder="E-mail" required /></p>
                    <!-- repeat email -->
                    <p>Powtórz e-mail:</p>
                    <p><input type="text" name="email2" placeholder="E-mail" required /></p>
                    <!-- php email problem alert -->
                    <?php

                    // email error info
                    echo "<p><span style='color: #ff0000; font-weight: bold'>" . $_SESSION['emailInfo'] . "</span></p>";
                    unset($_SESSION['emailInfo']);

                    ?>

                    <!-- password -->
                    <p>Wpisz hasło:</p>
                    <p><input type="password" name="password" placeholder="Hasło" required /></p>
                    <!-- repeat password -->
                    <p>Powtórz hasło:</p>
                    <p><input type="password" name="password2" placeholder="Hasło" required /></p>
                    <!-- php password problems alert -->
                    <?php

                    // password error info
                    echo "<p><span style='color: #ff0000; font-weight: bold'>".$_SESSION['passwordInfo']."</span></p>";
                    unset($_SESSION['passwordInfo']);

                    echo "<p><span style='color: #00ff00; font-weight: bold'>".$_SESSION['createdAccount']."</span></p>";
                    unset($_SESSION['createdAccount']);

                    ?>

                    <!-- submit button -->
                    <p><input type="submit" value="Stwórz konto!" /></p>

                    <!-- ,,Do you have account? Logi'' -->
                    <p>Masz już konto? <a id="account-span">Zaloguj się!</a></p>

                </form>

            </div>

        </div>

    </section>

</body>

</html>